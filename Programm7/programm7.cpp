#include <iostream> 
#include <ctime> 

using namespace std;

void quickSort(int *_leftHold, int *_rightHold);

int main(int argc, char **argv) {
	srand(time(NULL));
	unsigned sizeOfArray = rand() % 15 + 5;
	int *array = new int[sizeOfArray],
		*leftHold = &array[0],
		*rightHold = &array[sizeOfArray - 1];

	for (unsigned count = 0; count < sizeOfArray; ++count) {
		array[count] = rand() % 30 - 15;
		cout << array[count] << " ";
	}
	cout << "\n";

	quickSort(leftHold, rightHold);

	for (unsigned count = 0; count < sizeOfArray; ++count)
		cout << array[count] << " ";
	cout << "\n";

	delete[] array;

	system("pause");
	return 0;
}

void quickSort(int *_leftHold, int *_rightHold) {
	int resolv = *_leftHold,
		*temp_right = _rightHold,
		*temp_left = _leftHold;


	while (_leftHold < _rightHold) {

		while (*_rightHold >= resolv && _leftHold < _rightHold)
			_rightHold--;
		if (_leftHold != _rightHold) {
			*_leftHold = *_rightHold;
			_leftHold++;
		}

		while (*_leftHold <= resolv && _leftHold < _rightHold)
			_leftHold++;
		if (_leftHold != _rightHold) {
			*_rightHold = *_leftHold;
			_rightHold--;
		}
	}
	*_leftHold = resolv;
	int *tempPtr = _leftHold;
	_leftHold = temp_left;
	_rightHold = temp_right;

	if (_leftHold < tempPtr)
		quickSort(_leftHold, tempPtr + 1);
	if (_rightHold > tempPtr)
		quickSort(tempPtr + 1, _rightHold);
}